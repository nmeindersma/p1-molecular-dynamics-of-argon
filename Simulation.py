import numpy as np
import random
import matplotlib.pyplot as plt

#Set for fixed "random" numbers
np.random.seed(5)

#Parameters 
kB = 1.38064852 * 10**-23  #Boltzmann constant[m2 kg s-2 K-1] is used to compute Lennard-Jones potential
epsilon = 119.8 * kB #[m-2 kg-1 s2] is used to compute Lennard-Jones potential
sigma = 3.405e-10 #[m] is used to compute Lennard-Jones potential
m = 6.6335209e-26 #mass of one Argon molecule [kg]

h = 0.5 #timestep
N = 9 #number of particles
dim = 2 #number of dimensions
t = np.arange(0, 5, h) #time-array in units of sigma * sqrt(m/espilon)
L = 10 #boxsize for particles in units of sigma
v_0 = 1 # intial velocity scaling in units of sqrt(epsilon/m)

# Create position and velocity matrices
pos = np.zeros((dim,N,len(t))) 
# visualise as 3D matrix with particle number downward, time left right, x, y in depth.

velocity = np.zeros((dim,N,len(t))) 
# visualise as 3D matrix with particle number downward, time left right, V_x, v_y in depth.

# Matrix with distance between particle 1-> 2 downward, distance 2->1 left right
# All diagonal elements should be zero (distance to itself)
# Matrix should also be antisymmetric, as 1 -> 2 is opposite value of 2 -> 1
# !!!!! Optimise that those values do not need to be calculated
# Total matrix should be skew-symmetric
dx = np.zeros([N,N])
dy = np.zeros([N,N])

# Matrix for each energy type for N particles

U_pot = np.zeros([N,len(t)])
E_kin = np.zeros([N,len(t)])
E_total_n = np.zeros([N,len(t)])
E_total = np.zeros(len(t))

#Initialise problem 
for i in range(N):
    # Gives all dimenension different value
    for j in range(dim):
        # Set initial velocity, scaled by velocity scaling
        # Can be both negative and positive
        velocity[j,i,0]= random.uniform(-1, 1)*v_0
        # Set initial position, scaled by size of box
        # Only positive values so they are set inside
        pos[j,i,0]= np.random.random()*L
        
        
# Show intial placement particles
for n in range(N):
    plt.scatter(pos[0,n,0],pos[1,n,0], label = ("N = "+ str(n)+ ", t = "+str(0)))
plt.xlim(0,L)
plt.ylim(0,L)            
plt.legend()            
plt.show()  

# Create nesecarry functions
def distance(dx,dy):
    """This functions computes the distance between two particles"""
    distance = np.sqrt(dx**2 + dy**2) #distance between two particles
    return distance

def F(dx, dy):
    """This function computes the force on a particle due to the Lennard Jones potential in 2D in the xy-plane for a 2 particle-problem"""
    r = distance(dx, dy)
    a = 4 * ((12 * r**-13 - 6 * r**-7)) * 1/r  #equals -dU/dr * 1/r
    return [a*dx, a*dy]
    #represents force excerted on particle in the x,y plane
    # !!!!!!!
    # Check wether this is right or need other funtion for a * r

def energy_potential(r):
    """ This function computes the Lennard Jones energy potential with a distance between the two particles r"""
    U_pot = 4 * ((r**-12 - r**-6)) #definition of the Lennard Jones potential in units of epsilon
    return U_pot

def energy_kin(v):
    """ This function computes the kinectic energy of a particle with mass m and velocity v"""
    U_kin = 1/2 * v**2 #definition kinectic energy in units of epsilon
    return U_kin

# Calculate position and velocity for each timestep using Euler  
# i for each timestamp
for i in range(0, len(t)-1):   
   # n for each particle
    for n in range(N):
        # j for each dimension
        for j in range(dim):
            
            # particle n, j'th dimension at time i: if it exceeded the boundaries, bring back within boundaries 
            if pos[j,n,i] >= L: 
                pos[j,n,i] = pos[j,n,i] % L
            elif pos[j,n,i] < 0:
                pos[j,n,i] = L - abs(pos[j,n,i])%L
            else:
                pass
            
   
        # Loop trough all other particles to find relative distance
        for a in range(N):
            # Distance in x, between particle n and a
            dx[a,n]= (pos[0,a,i] - pos[0,n,i] + L/2) % L - L/2
            # Distance in y, between particle n and a
            dy[a,n]= (pos[0,a,i] - pos[0,n,i] + L/2) % L - L/2
        
        #compute the position and velocity of particle n at a time i
        for j in range(dim):
            pos[j,n,i+1] = pos[j,n,i] + velocity[j,n,i]* h  

        # Use loop to sum over all contributions of each seperate force
        for b in range(N):
            # Exclude force on itself
            if b !=n: 
                # Force on n is calculated using its (absolute) distance to  particle b
                velocity[0,n,i+1] = velocity[0,n,i]+ np.multiply(F(dx[b,n],dy[b,n])[0], h/m)
                velocity[1,n,i+1] = velocity[1,n,i]+ np.multiply(F(dx[b,n],dy[b,n])[1], h/m)

                # Sum over each contribution of potential energy of particle b on n at time i
                U_pot[n,i] += energy_potential(distance(dx[b,n],dy[b,n]))

        # Calculate kinetic energy of particle n at time i 
        E_kin[n,i] = energy_kin(distance(velocity[0,n,i],velocity[1,n,i]))

        # Create figure that shows each particles position over time
        plt.scatter(pos[0,n,i],pos[1,n,i], label = ("N = "+ str(n)+ ", t = "+str(i)))

    # Plot only one particle (N = 3) over time
    #plt.scatter(pos[0,2,i],pos[1,2,i], label = ("N = "+ str(3)+ ", t = "+str(i)))    

    

    
    
# Plotting all the particles within the box at all times 
plt.xlim(0,L)
plt.ylim(0,L)            
#plt.legend()            
plt.show()





# Calculate total energy at each time 
for i in range(len(t)):
    # For each particle n at time i
    E_total_n[:,i] = U_pot[:,i]+ E_kin[:,i]
    
    # Total energy of the system at time i
    E_total[i] = np.linalg.norm(E_total_n[:,i])    


    
# To check wether are actualy Skew-symmetric matrices
print(dx) 
print(dy)

# Print energy
print("Kin")
print(E_kin)
print("pot")
print(U_pot)   
print("E_total_n")
print(E_total_n)
print("E_total")
print(E_total)



#plt.plot(U_total, label ="total")
# for n in range(N):
#     plt.plot(U_pot[n,:], label ="pot")
# plt.show()

# plt.plot(E_kin, label ="kin")
# plt.show()
#plt.legend()

# print("Velocity:")
# print(velocity)
                
# print("pos:")
# print(pos)