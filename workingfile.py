# working file: making the first steps of the project

import numpy as np

#Parameters
kB = 1.38064852 * 10**-23  #Boltzmann constant[m2 kg s-2 K-1] is used to compute Lennard-Jones potential
epsilon = 119.8 * kB #[m-2 kg-1 s2] is used to compute Lennard-Jones potential
sigma = 3.405e-10 #[m] is used to compute Lennard-Jones potential

h = 0.5 #timestep
N = 2 #number of particles
dim = 2 #number of dimensions
m = 6.6335209e-26 #mass of one Argon molecule [kg]
t = np.arange(0, 5, h) #time-array
L = 10 *sigma #boxsize for particles

#Initialise problem 
pos1 = np.zeros((len(t)+1, dim)) #creating position array of size time-array of particle 1
pos2 = np.zeros((len(t)+1, dim)) #creating position array of size time-array of particle 2
v1 = np.zeros((len(t)+1, dim)) #velocity-array of size time-array of particle 1
v2 = np.zeros((len(t)+1, dim)) #velocity-array of size time-array of particle 2

U_total= np.zeros(len(t)) #represents total energy of this system
E_pot= np.zeros(len(t))  #represents potential energy of this system
E_kin = np.zeros(len(t)) #represents kinetic energy of this system

#Set initial position and velocity (this can be randomised at a later time)
pos1_0 = [1*sigma, 1*sigma] #initial position [x,y] of particle 1 in the x,y plane
pos2_0 = [2*sigma, 2*sigma] #initial position [x,y] of particle 2 in the x,y plane
v1_0 = [0, 0] #initial velocity [vx,vy] of particle 1 in the x,y plane
v2_0 = [0, 0] #initial velocity [vx,vy] of particle 2 in the x,y plane

pos1[0] = pos1_0 #input initial position of particle 1
pos2[0] = pos2_0 #input initial position of particle 2
v1[0] = v1_0 #input initial velocity of particle 1
v2[0] = v2_0 #input initial velocity of particle 2

def distance(dx,dy):
    """This functions computes the distance between two particles"""
    distance = np.sqrt(dx**2 + dy**2) #distance between two particles
    return distance

def F(dx, dy):
    """This function computes the force on a particle due to the Lennard Jones potential in 2D in the xy-plane for a 2 particle-problem"""
    r = distance(dx, dy) #distance between two particles
    a = 4*epsilon * ((12 * sigma**12 * r**-13 - 6 * sigma**6 * r**-7)) * 1/r  #equals -dU/dr * 1/r
    return [a*dx, a*dy] #represents force excerted on particle in the x,y plane

def energy_potential(dx,dy):
    """ This function computes the Lennard Jones energy potential with a distance between the two particles r"""
    r = distance(dx, dy) #distance between two particles
    U_pot = 4*epsilon * ((sigma**12 * r**-12 - sigma**6 * r**-6)) #definition of the Lennard Jones potential
    return U_pot

def energy_kin(v):
    """ This function computes the kinectic energy of a particle with mass m and velocity v"""
    U_kin = 1/2*m* np.sqrt(v[0]**2+v[1]**2) #definition kinectic energy
    return U_kin


for i in range(0, len(t)-1): #calculate position and velocity for each timestep using Euler    
#     if pos1[i,0] >= L: # particle 1, x pos: if it exceeded the boundaries, bring back within boundaries 
#         pos1[i,0] = pos1[i,0] % L
#     elif pos1[i,0] < 0:
#         pos1[i,0] = L - abs(pos1[i,0])%L
#     else:
#         pass
    
#     if pos1[i,1] >= L: # particle 1, y pos
#         pos1[i,1] = pos1[i,1] % L
#     elif pos1[i,1] < 0:
#         pos1[i,1] = L - abs(pos1[i,1])%L
#     else:
#         pass
    
#     if pos2[i,0] >= L: # particle 2, x pos
#         pos2[i,0] = pos2[i,0] % L
#     elif pos2[i,0] < 0:
#         pos2[i,0] = L - abs(pos2[i,0])%L
#     else:
#         pass
    
#     if pos2[i,1] >= L: # particle 2, y pos
#         pos2[i,1] = pos2[i,0] % L
#     elif pos2[i,1] < 0:
#         pos2[i,1] = L - abs(pos2[i,1])%L
#     else:
#         pass
    
    pos1[i,0] = pos1[i,0] % L  # particle 1, x pos: if it exceeded the boundaries, bring back within boundaries 
    pos1[i,1] = pos1[i,1] % L  # particle 1, y pos
    pos2[i,1] = pos2[i,0] % L  # particle 1, x pos: if it exceeded the boundaries, bring back within boundaries 
    pos2[i,1] = pos2[i,0] % L  # particle 1, y pos

    #compute the realitve distance of the two particles for x and y coordinate at a timestep i
    dx = pos1[i, 0] - pos2[i, 0] #Distance between the two particles in x direction
    dy = pos1[i, 1] - pos2[i, 1] #Distance between the two particles in y direction
    
    #compute the position and velocity of the particles at a time i
    pos1[i+1] = pos1[i] + v1[i]* h  
    pos2[i+1] = pos2[i] + v2[i]* h
    v1[i+1] = v1[i] + np.multiply(F(dx, dy), h/m) #use numpy to multiply array by scalar (how to make it more compact???)
    v2[i+1] = v2[i] + np.multiply(F(-dx, -dy), h/m)
    
    #compute potential and kinetic energy of the system at a time i
    E_pot[i] = energy_potential(dx,dy)
    E_kin[i] = energy_kin(v1[i])+energy_kin(v2[i])    
    

#plotting the systems energy    
import matplotlib.pyplot as plt
U_total = E_pot+ E_kin
plt.plot(U_total, label ="total")
plt.plot(E_pot, label ="pot")
plt.plot(E_kin, label ="kin")
plt.legend()

#plot the positions of the two particles
#plot the positions of the two particles
plt.plot(pos1[:-2,0],pos1[:-2,1], "-o", label = "1")
plt.plot(pos2[:-2,0],pos2[:-2,1],  "-o", label = "2")
plt.plot(pos2[0,0],pos2[0,1],  "-o", label = "2_0")
plt.legend()